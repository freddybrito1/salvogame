package com.codeoftheweb.salvo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.*;
import java.util.*;

@Entity
public class GamePlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    Set<Ship> ships = new HashSet<>();

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    Set<Salvo> salvoes = new HashSet<>();

    private Date joinDate;

    /* Constructors */

    public GamePlayer() {
        this.joinDate =   new Date();
    }

    public GamePlayer(Date joinDate, Player player, Game game) {
        this.player = player;
        this.game = game;
    }

    public GamePlayer(Player player, Game game) {
        this.joinDate = new Date();
        this.player = player;
        this.game = game;
    }
    /* Getter and Setter */

    public long getId() {
        return id;
    }

    @JsonIgnore
    public Player getPlayer() {
        return player;
    }


@JsonIgnore
    public Game getGame() {
        return game;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public Set<Salvo> getSalvoes() {
        return salvoes;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }



    public Map<String, Object> makeGamePlayerDTO(){
        Map<String, Object> dto =   new HashMap<>();
        dto.put("id",   this.getId());
        dto.put("player",   this.getPlayer().makePlayerDTO());
        return  dto;
    }
}
