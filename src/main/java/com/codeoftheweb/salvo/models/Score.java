package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


@Entity
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private Double score;
    private Date finishDate;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

    public Score() {
    }

    public Score( Game game, Player player, Double score, Date finishDate) {
        this.score = score;
        this.finishDate = finishDate;
        this.game = game;
        this.player = player;
    }

    public Map<String,  Object> makeScoreDTO(){
        Map<String,  Object>    dto=    new LinkedHashMap<>();
        dto.put("player",   this.getPlayer().getId());
        dto.put("score",   this.getScore());
        dto.put("finishDate", this.getFinishDate().getTime());
        return  dto;
    }

    public long getId() {
        return id;
    }

    public Double getScore() {
        return score;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public Game getGame() {
        return game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }
}
