package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Null;
import java.util.Arrays;
import java.util.Date;


@SpringBootApplication
public class SalvoApplication {


    public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
    public CommandLineRunner initData(GamePlayerRepository  gamePlayerRepository, PlayerRepository playerRepository, GameRepository gameRepository, ShipRepository shipRepository, SalvoRepository salvoRepository, ScoreRepository scoreRepository) {
        return (args) -> {



/*

            String carrier = "carrier";

            String battleship = "battleship";

            String submarine = "submarine";

            String destroyer = "destroyer";

            String patrolBoat = "patrolboat";



            Date newDate1 =  new Date();




            Player player1 = new Player("j.bauer@ctu.gov", passwordEncoder().encode("24"));
            Player player2 = new Player("c.obrian@ctu.gov", passwordEncoder().encode("42"));


            playerRepository.save(player1);
            playerRepository.save(player2);


            Game game1 = new Game(newDate1);


            gameRepository.save(game1);

            GamePlayer  gamePlayer1 = new GamePlayer(newDate1, player1, game1);
            GamePlayer gamePlayer2 = new GamePlayer(newDate1, player2, game1);


            gamePlayerRepository.save(gamePlayer1);
            gamePlayerRepository.save(gamePlayer2);



            Ship ship1 = new Ship(gamePlayer1, destroyer, Arrays.asList("H2", "H3", "H4"));

            Ship ship2 = new Ship(gamePlayer1, submarine, Arrays.asList("E1", "F1", "G1"));

            Ship ship3 = new Ship(gamePlayer1, patrolBoat, Arrays.asList("B4", "B5"));

            Ship ship4 = new Ship(gamePlayer2, carrier, Arrays.asList("B5", "C5", "D5"));

            Ship ship5 = new Ship(gamePlayer2, battleship, Arrays.asList("F1", "F2"));



            shipRepository.save(ship1);

            shipRepository.save(ship2);

            shipRepository.save(ship3);

            shipRepository.save(ship4);

            shipRepository.save(ship5);




            Salvo salvo1 = new Salvo(gamePlayer1, 1, Arrays.asList("B4", "B5", "B6", "A3", "D5"));
            salvoRepository.save(salvo1);

            Salvo salvo2 = new Salvo(gamePlayer2, 1, Arrays.asList("C4", "C5", "C6", "A1", "B4"));
            salvoRepository.save(salvo2);
*/

        };


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}


@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    PlayerRepository playerRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(inputName-> {
            Player player = playerRepository.findByUserName(inputName).get();
            if (player != null) {
                return new User(player.getUserName(), player.getPassword(),
                        AuthorityUtils.createAuthorityList("USER"));
            } else {
                throw new UsernameNotFoundException("Unknown user: " + inputName);

            }
        });
    }
}

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/game_view/**").hasAuthority("USER")
                .antMatchers("/api/games").permitAll()
                .antMatchers("/web/**").permitAll()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/h2-console/**").permitAll()

                .antMatchers("/rest/**").permitAll() //Al Finalizar todo denyAll()
               // .antMatchers("/admin/**").hasAuthority("ADMIN")

        .anyRequest().permitAll();

        http.formLogin()
                .usernameParameter("userName")
                .passwordParameter("password")
                .loginPage("/api/login");

        http.logout().logoutUrl("/api/logout");



        // turn off checking for CSRF tokens
        http.csrf().disable();
        http.headers().frameOptions().disable(); // Enable web h2 console

        // if user is not authenticated, just send an authentication failure response
        http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if login is successful, just clear the flags asking for authentication
        http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

        // if login fails, just send an authentication failure response
        http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if logout is successful, just send a success response
        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());


    }

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }
    }
}