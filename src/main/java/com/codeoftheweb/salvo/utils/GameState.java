package com.codeoftheweb.salvo.utils;

public enum GameState {

    WAITINGFOROPP,
    WAIT,
    PLAY,
    PLACESHIPS,
    WON,
    LOST,
    TIE,
    UNDEFINED

}
