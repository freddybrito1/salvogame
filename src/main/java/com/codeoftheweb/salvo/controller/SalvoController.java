package com.codeoftheweb.salvo.controller;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.models.Salvo;
import com.codeoftheweb.salvo.repository.GamePlayerRepository;
import com.codeoftheweb.salvo.repository.GameRepository;
import com.codeoftheweb.salvo.repository.PlayerRepository;
import com.codeoftheweb.salvo.repository.SalvoRepository;
import com.codeoftheweb.salvo.utils.GameState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PlayerRepository playerRepository;


    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    @RequestMapping("/games")
    public Map<String, Object> getGameAll(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();

        if (isGuest(authentication)) {
            dto.put("player", "Guest");
        } else {
            Player player = playerRepository.findByUserName(authentication.getName()).get();
            dto.put("player", player.makePlayerDTO());
        }
        dto.put("game", gameRepository.findAll()
                .stream()
                .map(game -> game.makeGameDTO())
                .collect(Collectors.toList()));
        return dto;
    }

    @RequestMapping("/leaderBoard")
    public List<Map<String, Object>> leaderBoard() {
        return playerRepository.findAll()
                .stream()
                .map(player -> player.makePlayerScoreDTO())
                .collect(Collectors.toList());
    }

    /*  THIS IT FOR NO USE.  ORIGINAL  CODE
    @RequestMapping("/game_view/{nn}")
    public Map<String, Object> getGameViewByGamePlayerID(@PathVariable Long nn) {

        GamePlayer gamePlayer = gamePlayerRepository.findById(nn).get();

        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayers()
                .stream()
                .map(gamePlayer1 -> gamePlayer1.makeGamePlayerDTO())
                .collect(Collectors.toList()));
        dto.put("ships", gamePlayer.getShips()
                .stream()
                .map(ship -> ship.makeShipDTO())
                .collect(Collectors.toList()));

        dto.put("salvoes", gamePlayer.getGame().getGamePlayers().stream()
                .flatMap(gamePlayer1 -> gamePlayer1.getSalvoes()
                        .stream()
                        .map(salvo -> salvo.makeSalvoDTO())
                )
                .collect(Collectors.toList())
        );
        return dto;
    }


     */

    // LAST EDITION FOR TASK 2 MODULE JAVA2

    @RequestMapping("/game_view/{nn}")
    public ResponseEntity<Map<String, Object>> getGameViewByGamePlayerID(@PathVariable Long nn, Authentication authentication) {

        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "User No Authorized"), HttpStatus.UNAUTHORIZED);
        }

        Player player = playerRepository.findByUserName(authentication.getName()).orElse(null);
        GamePlayer gamePlayer = gamePlayerRepository.findById(nn).orElse(null);

        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "Player Null"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Game Player Null"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer.getPlayer().getId() != player.getId()) {
            return new ResponseEntity<>(makeMap("error", "It's No the Game for this Player"), HttpStatus.CONFLICT);
        }


        Map<String, Object> dto = new LinkedHashMap<>();
        Map<String, Object> hits = new LinkedHashMap<>();
        // Test from here



        //  hits.put("self", new ArrayList<>());  FROM HERE!!!
        //  hits.put("opponent", new ArrayList<>());

        hits.put("self", gamePlayer.getShips().isEmpty()    ?   new ArrayList<>()   :   getHits(gamePlayer, getOpponent(gamePlayer)));
        hits.put("opponent", getOpponent(gamePlayer).getShips().isEmpty()   ?   new ArrayList<>()   :   getHits(getOpponent(gamePlayer), gamePlayer));

        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gameState",getGameState(gamePlayer) );

        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayers()
                .stream()
                .map(gamePlayer1 -> gamePlayer1.makeGamePlayerDTO())
                .collect(Collectors.toList()));
        dto.put("ships", gamePlayer.getShips()
                .stream()
                .map(ship -> ship.makeShipDTO())
                .collect(Collectors.toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayers()
                .stream()
                .flatMap(gamePlayer1 -> gamePlayer1.getSalvoes()
                        .stream()
                        .map(salvo -> salvo.makeSalvoDTO()))
                .collect(Collectors.toList()));
        dto.put("hits", hits);


        return new ResponseEntity<>(dto, HttpStatus.OK);
    }



    @RequestMapping(path = "/game/{gameID}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> joinGame(@PathVariable Long gameID, Authentication authentication) {
        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "You can't join a Game if You're Not Logged In!"), HttpStatus.UNAUTHORIZED);
        }

        Player player = playerRepository.findByUserName(authentication.getName()).orElse(null);
        Game gameToJoin = gameRepository.getOne(gameID);


        if (gameRepository.getOne(gameID) == null) {
            return new ResponseEntity<>(makeMap("error", "No such game."), HttpStatus.FORBIDDEN);
        }

        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "No such game."), HttpStatus.FORBIDDEN);
        }

        long gamePlayersCount = gameToJoin.getGamePlayers().size();

        if (gamePlayersCount == 1) {
            GamePlayer gameplayer = gamePlayerRepository.save(new GamePlayer(player, gameToJoin));
            return new ResponseEntity<>(makeMap("id", gameplayer.getId()), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(makeMap("error", "Game is full!"), HttpStatus.FORBIDDEN);
        }

    }


    @RequestMapping(path = "/games/players/{gpid}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Map> addSalvoes(@PathVariable Long gpid, @RequestBody Salvo salvo, Authentication authentication) {

        GamePlayer gamePlayer = gamePlayerRepository.findById(gpid).get();

        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "User No Authorized"), HttpStatus.UNAUTHORIZED);
        }

        Player player = playerRepository.findByUserName(authentication.getName()).get();

        if (gamePlayer == null) {

            return new ResponseEntity<>(makeMap("error", "Guess can't do it"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer.getPlayer().getId() != player.getId()) {

            return new ResponseEntity<>(makeMap("error", "Is no the same Player"), HttpStatus.FORBIDDEN);
        }

        if (gamePlayer.getSalvoes().size() <= getOpponent(gamePlayer).getSalvoes().size()) {
            salvo.setTurn(gamePlayer.getSalvoes().size() + 1);
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);
            return new ResponseEntity<>(makeMap("OK", "Salvo created"), HttpStatus.CREATED);
        }

        return new ResponseEntity<>(makeMap("OK", "Salvo Send Everythink it's ok"), HttpStatus.CREATED);
    }

    public GamePlayer getOpponent(GamePlayer gamePlayer) {
        return gamePlayer.getGame().getGamePlayers().stream().filter(gamePlayer1 -> gamePlayer1.getId() != gamePlayer.getId()).findFirst().orElse(new GamePlayer());

    }

    public List<Map> getHits(GamePlayer gamePlayerSelf, GamePlayer gamePlayerOpponent){

        List<Map> hits = new ArrayList<>();

        List<String> patrolboatLocations =  gamePlayerSelf.getShips().stream().filter(ship -> ship.getType().equals("patrolboat")).findFirst().get().getShipLocations();
        List<String> carrierLocations =  gamePlayerSelf.getShips().stream().filter(ship -> ship.getType().equals("carrier")).findFirst().get().getShipLocations();
        List<String> submarineLocations =  gamePlayerSelf.getShips().stream().filter(ship -> ship.getType().equals("submarine")).findFirst().get().getShipLocations();
        List<String> destroyerLocations =  gamePlayerSelf.getShips().stream().filter(ship -> ship.getType().equals("destroyer")).findFirst().get().getShipLocations();
        List<String> battleshipLocations =  gamePlayerSelf.getShips().stream().filter(ship -> ship.getType().equals("battleship")).findFirst().get().getShipLocations();


        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0;



        for (Salvo salvo: gamePlayerOpponent.getSalvoes()) {

            long carrierHitsTurn = 0;
            long battleshipHitsTurn = 0;
            long submarineHitsTurn = 0;
            long destroyerHitsTurn = 0;
            long patrolboatHitsTurn = 0;
            long missed = salvo.getSalvoLocations().size();


            Map<String, Object> hitsPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesTurn = new LinkedHashMap<>();

            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();



     for (String location: salvo.getSalvoLocations()){

         if( carrierLocations.contains(location)){
             carrierDamage++;
             carrierHitsTurn++;
             hitCellsList.add(location);
             missed--;

         }
         if (battleshipLocations.contains(location)){
             battleshipDamage++;
             battleshipHitsTurn++;
             hitCellsList.add(location);
             missed--;
         }
         if(submarineLocations.contains(location)){
             submarineDamage++;
             submarineHitsTurn++;
             hitCellsList.add(location);
             missed--;
         }
         if(destroyerLocations.contains(location)){
             destroyerDamage++;
             destroyerHitsTurn++;
             hitCellsList.add(location);
             missed--;
         }
         if(patrolboatLocations.contains(location)){
             patrolboatDamage++;
             patrolboatHitsTurn++;
             hitCellsList.add(location);
             missed--;
         }
     }

            damagesTurn.put("carrierHits", carrierHitsTurn);
            damagesTurn.put("battleshipHits", battleshipHitsTurn);
            damagesTurn.put("submarineHits", submarineHitsTurn);
            damagesTurn.put("destroyerHits", destroyerHitsTurn);
            damagesTurn.put("patrolboatHits", patrolboatHitsTurn);
            damagesTurn.put("carrier", carrierDamage);
            damagesTurn.put("battleship", battleshipDamage);
            damagesTurn.put("submarine", submarineDamage);
            damagesTurn.put("destroyer", destroyerDamage);
            damagesTurn.put("patrolboat", patrolboatDamage);

            hitsPerTurn.put("turn", salvo.getTurn());
            hitsPerTurn.put("hitLocations", hitCellsList);
            hitsPerTurn.put("damages", damagesTurn);
            hitsPerTurn.put("missed", missed);
            hits.add(hitsPerTurn);

 };

return hits;

}

    private GameState getGameState (GamePlayer gamePlayer) {

        if (gamePlayer.getShips().size() == 0) {
            return GameState.PLACESHIPS;
        }
        if (gamePlayer.getGame().getGamePlayers().size() == 1){
            return GameState.WAITINGFOROPP;
        }
        if (gamePlayer.getGame().getGamePlayers().size() == 2) {

            GamePlayer opponentGp = getOpponent(gamePlayer);

            if ((gamePlayer.getSalvoes().size() == opponentGp.getSalvoes().size()) && (getIfAllSunk(opponentGp, gamePlayer)) && (!getIfAllSunk(gamePlayer, opponentGp))) {
                return GameState.WON;
            }
            if ((gamePlayer.getSalvoes().size() == opponentGp.getSalvoes().size()) && (getIfAllSunk(opponentGp, gamePlayer)) && (getIfAllSunk(gamePlayer, opponentGp))) {
                return GameState.TIE;
            }
            if ((gamePlayer.getSalvoes().size() == opponentGp.getSalvoes().size()) && (!getIfAllSunk(opponentGp, gamePlayer)) && (getIfAllSunk(gamePlayer, opponentGp))) {
                return GameState.LOST;
            }

            if ((gamePlayer.getSalvoes().size() == opponentGp.getSalvoes().size()) && (gamePlayer.getId() < opponentGp.getId())) {
                return GameState.PLAY;
            }
            if (gamePlayer.getSalvoes().size() < opponentGp.getSalvoes().size()){
                return GameState.PLAY;
            }
            if ((gamePlayer.getSalvoes().size() == opponentGp.getSalvoes().size()) && (gamePlayer.getId() > opponentGp.getId())) {
                return GameState.WAIT;
            }
            if (gamePlayer.getSalvoes().size() > opponentGp.getSalvoes().size()){
                return GameState.WAIT;
            }

        }
        return GameState.UNDEFINED;
    }

    private Boolean getIfAllSunk (GamePlayer self, GamePlayer opponent) {

        if(!opponent.getShips().isEmpty() && !self.getSalvoes().isEmpty()){
            return opponent.getSalvoes().stream().flatMap(salvo -> salvo.getSalvoLocations().stream()).collect(Collectors.toList()).containsAll(self.getShips().stream()
                    .flatMap(ship -> ship.getShipLocations().stream()).collect(Collectors.toList()));
        }
        return false;
    }





}